<?php

namespace App\Tests\Service\Reservation;

use App\Entity\FoodTruck;
use App\Entity\Reservation;
use App\Repository\ReservationRepository;
use App\Service\Reservation\ValidateReservationInterface;
use App\Service\Reservation\ValidateReservationService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ValidateReservationServiceTest extends KernelTestCase
{

    public function testIsInThePastOrTodayWithDateInThePast()
    {
        $service = $this->initService();
        $date = (new \DateTimeImmutable())->sub(\DateInterval::createFromDateString("1 days"));
        $reservation = (new Reservation())->setBookedOn($date)->setFoodTruck(new FoodTruck());

        $bool = $service->isInThePastOrToday($reservation);
        $this->assertEquals(true, $bool);
    }

    public function testIsInThePastOrTodayWithDateToday()
    {
        $service = $this->initService();
        $reservation = (new Reservation())->setBookedOn(new \DateTimeImmutable())->setFoodTruck(new FoodTruck());
        $bool = $service->isInThePastOrToday($reservation);
        $this->assertEquals(true, $bool);
    }

    public function testIsInThePastOrTodayWithDateInTheFuture()
    {
        $service = $this->initService();
        $date = (new \DateTimeImmutable())->add(\DateInterval::createFromDateString("1 days"));
        $reservation = (new Reservation())->setBookedOn($date)->setFoodTruck(new FoodTruck());
        $bool = $service->isInThePastOrToday($reservation);
        $this->assertEquals(false, $bool);
    }

    public function testIsFridayWithNotFriday()
    {
        $service = $this->initService();
        $reservation = (new Reservation())->setBookedOn(new \DateTimeImmutable("2022-04-20"))->setFoodTruck(new FoodTruck());
        $bool = $service->isFriday($reservation);
        $this->assertEquals(false, $bool);
    }

    public function testIsFridayWithGoodDay()
    {
        $service = $this->initService();
        $reservation = (new Reservation())->setBookedOn(new \DateTimeImmutable("2022-04-22"))->setFoodTruck(new FoodTruck());

        $bool = $service->isFriday($reservation);
        $this->assertEquals(true, $bool);
    }

    public function testIsCompleteWithMoreThanSevenReservations()
    {
        $reservation = (new Reservation())->setBookedOn(new \DateTimeImmutable());
        $repository = $this->getMockBuilder(ReservationRepository::class)->disableOriginalConstructor()->getMock();
        $repository->method("countByDate")->willReturn(8);

        $service = $this->getMockBuilder(ValidateReservationService::class)->setConstructorArgs([$repository])->onlyMethods(["isFriday"])->getMock();
        $service->method("isFriday")->willReturn(false);

        $this->assertEquals(true, $service->isComplete($reservation));
    }

    public function testIsCompleteWithLessThanSevenReservations()
    {
        $reservation = (new Reservation())->setBookedOn(new \DateTimeImmutable());
        $repository = $this->getMockBuilder(ReservationRepository::class)->disableOriginalConstructor()->getMock();
        $repository->method("countByDate")->willReturn(6);

        $service = $this->getMockBuilder(ValidateReservationService::class)->setConstructorArgs([$repository])->onlyMethods(["isFriday"])->getMock();
        $service->method("isFriday")->willReturn(false);

        $this->assertEquals(false, $service->isComplete($reservation));
    }

    public function testIsCompleteWithSevenReservations()
    {
        $reservation = (new Reservation())->setBookedOn(new \DateTimeImmutable());
        $repository = $this->getMockBuilder(ReservationRepository::class)->disableOriginalConstructor()->getMock();
        $repository->method("countByDate")->willReturn(7);
        $service = $this->getMockBuilder(ValidateReservationService::class)->setConstructorArgs([$repository])->onlyMethods(["isFriday"])->getMock();
        $service->method("isFriday")->willReturn(false);

        $this->assertEquals(true, $service->isComplete($reservation));
    }

    public function testIsCompleteWithSixReservationsOnFriday()
    {
        $reservation = (new Reservation())->setBookedOn(new \DateTimeImmutable());
        $repository = $this->getMockBuilder(ReservationRepository::class)->disableOriginalConstructor()->getMock();
        $repository->method("countByDate")->willReturn(6);

        $service = $this->getMockBuilder(ValidateReservationService::class)->setConstructorArgs([$repository])->onlyMethods(["isFriday"])->getMock();
        $service->method("isFriday")->willReturn(true);

        $this->assertEquals(true, $service->isComplete($reservation));
    }

    private function initService(): ValidateReservationInterface
    {
        self::bootKernel();
        $reservationRepository = $this->getMockBuilder(ReservationRepository::class)->disableOriginalConstructor()->getMock();
        return new ValidateReservationService($reservationRepository);
    }
}
