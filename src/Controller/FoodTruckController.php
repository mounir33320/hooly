<?php

namespace App\Controller;

use App\Entity\FoodTruck;
use App\Service\FoodTruck\FoodTruckService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

#[Route("/api/food-trucks", name: "food-trucks_")]
class FoodTruckController extends AbstractController
{
    public function __construct(
        private SerializerInterface $serializer,
        private NormalizerInterface $normalizer,
        private FoodTruckService $foodTruckService
    )
    {
    }

    /**
     * @throws ExceptionInterface
     */
    #[Route("", name: "index", methods: ["GET"])]
    public function index(): JsonResponse
    {
        $allFoodTruck = $this->foodTruckService->getAll();

        $normalizedAllFoodTruck = $this->normalizer->normalize($allFoodTruck, "json", ["groups" => ["read:collection:FoodTruck"]]);

        return new JsonResponse(["success" => true, "data" => $normalizedAllFoodTruck]);
    }

    /**
     * @throws ExceptionInterface
     */
    #[Route("/{id}", name: "show", methods: ["GET"])]
    public function show(FoodTruck $foodTruck): JsonResponse
    {
        $normalizedFoodTruck = $this->normalizer->normalize($foodTruck, "json", ["groups" => ["read:item:FoodTruck"]]);

        return new JsonResponse(["success" => true, "data" => $normalizedFoodTruck]);
    }

    /**
     * @throws ExceptionInterface
     */
    #[Route("", name: "create", methods: ["POST"])]
    public function create(Request $request): JsonResponse
    {
        $data = $request->getContent();
        $newFoodTruck = $this->serializer->deserialize($data, FoodTruck::class, "json");

        $this->foodTruckService->save($newFoodTruck);

        $normalizedFoodTruck = $this->normalizer->normalize($newFoodTruck, "json", ["groups" => "read:item:FoodTruck"]);

        return new JsonResponse(["success" => true, "data" => $normalizedFoodTruck], Response::HTTP_CREATED);
    }

    /**
     * @throws ExceptionInterface
     */
    #[Route("/{id}", name: "edit", methods: ["PUT"])]
    public function edit(FoodTruck $foodTruck, Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        try {
            $foodTruck->setName($data["name"]);
            $this->foodTruckService->update($foodTruck);
        }
        catch (\Exception $exception){
            throw new BadRequestException("Les données envoyées ne sont pas valides");
        }

        $normalizedFoodTruck = $this->normalizer->normalize($foodTruck, "json", ["groups" => ["read:item:FoodTruck"]]);

        return new JsonResponse(["success" => true, "data" => $normalizedFoodTruck]);
    }

    #[Route("/{id}", name: "delete", methods: ["DELETE"])]
    public function delete(FoodTruck $foodTruck): JsonResponse
    {
        $this->foodTruckService->delete($foodTruck);
        return new JsonResponse(["success" => true]);
    }
}
