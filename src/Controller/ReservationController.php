<?php

namespace App\Controller;

use App\Entity\Reservation;
use App\Service\Reservation\ReservationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/reservations', name: 'reservations_')]
class ReservationController extends AbstractController
{
    public function __construct(
        private SerializerInterface $serializer,
        private NormalizerInterface $normalizer,
        private ReservationService $reservationService
    )
    {
    }

    /**
     * @throws ExceptionInterface
     */
    #[Route("", name: "index", methods: ["GET"])]
    public function index(): JsonResponse
    {
        $reservations = $this->reservationService->getAll();

        $normalizedReservations = $this->normalizer->normalize($reservations, "json", ["groups" => ["read:collection:Reservation"]]);

        return new JsonResponse(["success" => true, "data" => $normalizedReservations]);
    }

    #[Route("", name: "create", methods: ["POST"])]
    public function create(Request $request): JsonResponse
    {
        $data = $request->getContent();
            $newReservation = $this->serializer->deserialize($data, Reservation::class, "json");

        $this->reservationService->save($newReservation);
        $normalizedReservation = $this->normalizer->normalize($newReservation, "json", ["groups" => ["read:item:Reservation"]]);

        return new JsonResponse(["success" => true, "data" => $normalizedReservation], Response::HTTP_CREATED);
    }

    #[Route("/{id}", name: "delete", methods: ["DELETE"])]
    public function delete(Reservation $reservation): JsonResponse
    {
        $this->reservationService->delete($reservation);

        return new JsonResponse(["success" => true]);
    }
}
