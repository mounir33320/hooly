<?php

namespace App\Serializer;

use App\Entity\Reservation;
use App\Service\FoodTruck\FoodTruckService;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ReservationDenormalizer implements DenormalizerInterface, CacheableSupportsMethodInterface
{
    public function __construct(private ObjectNormalizer $normalizer, private FoodTruckService $foodTruckService)
    {
    }

    /**
     * @inheritDoc
     */
    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        if(
            !isset($data["foodTruck"]) ||
            !isset($data["foodTruck"]["id"]) ||
            !isset($data["bookedOn"])
        ){
            throw new BadRequestException("Les données envoyées ne sont pas correctes");
        }

        $foodTruck = $this->foodTruckService->get($data["foodTruck"]["id"]);

        return (new Reservation())
            ->setBookedOn(new \DateTimeImmutable($data["bookedOn"]))
            ->setFoodTruck($foodTruck)
            ;
    }

    /**
     * @inheritDoc
     */
    public function supportsDenormalization($data, string $type, string $format = null): bool
    {
        return $type == Reservation::class;
    }

    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }
}
