<?php

namespace App\Repository;

use App\Entity\FoodTruck;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FoodTruck|null find($id, $lockMode = null, $lockVersion = null)
 * @method FoodTruck|null findOneBy(array $criteria, array $orderBy = null)
 * @method FoodTruck[]    findAll()
 * @method FoodTruck[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FoodTruckRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FoodTruck::class);
    }

    public function add(FoodTruck $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function update(FoodTruck $foodTruck)
    {
        $this->_em->flush();
    }

    public function remove(FoodTruck $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
}
