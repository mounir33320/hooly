<?php

namespace App\Repository;

use App\Entity\FoodTruck;
use App\Entity\Reservation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Reservation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reservation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reservation[]    findAll()
 * @method Reservation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReservationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reservation::class);
    }

    public function add(Reservation $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove(Reservation $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function countByDate(\DateTimeInterface $dateTime): int
    {
        return (int) $this->createQueryBuilder("reservation")
            ->select("COUNT(reservation)")
            ->where("DATE_FORMAT(reservation.bookedOn, '%d-%m-%Y') = :bookedOn")
            ->setParameter("bookedOn", $dateTime->format("d-m-Y"))
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function countByFoodTruckAndBetweenTwoDateTime(\DateTimeInterface $firstDate, \DateTimeInterface $lastDate, Reservation $reservation): int
    {
        return (int) $this->createQueryBuilder("reservation")
            ->select("COUNT(reservation)")
            ->where("reservation.bookedOn BETWEEN :firstDate AND :lastDate")
            ->andWhere("reservation.foodTruck = :foodTruck")
            ->setParameters([
                "firstDate" => $firstDate,
                "lastDate" => $lastDate,
                "foodTruck" => $reservation->getFoodTruck()
            ])
            ->getQuery()
            ->getSingleScalarResult();
    }

}
