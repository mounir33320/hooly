<?php

namespace App\Service\FoodTruck;

use App\Entity\FoodTruck;
use App\Repository\FoodTruckRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

class FoodTruckService
{
    public function __construct(
        private FoodTruckRepository $foodTruckRepository,
    )
    {
    }

    public function getAll(): array
    {
        return $this->foodTruckRepository->findAll();
    }

    public function get(int $foodTruckId): FoodTruck
    {
        $foodTruck = $this->foodTruckRepository->find($foodTruckId);

        if(!$foodTruck){
            throw new ResourceNotFoundException("Ce food truck n'existe pas");
        }

        return $foodTruck;
    }

    public function save(FoodTruck $foodTruck)
    {
        $this->foodTruckRepository->add($foodTruck);
    }

    public function update(FoodTruck $foodTruck)
    {
        $this->foodTruckRepository->update($foodTruck);
    }

    public function delete(FoodTruck $foodTruck)
    {
        $this->foodTruckRepository->remove($foodTruck);
    }
}
