<?php

namespace App\Service\Reservation;

use App\DateHelper;
use App\Entity\Reservation;
use App\Exception\AlreadyReservedThisWeek;
use App\Exception\ReservationConstraintsException;
use App\Exception\ReservationDateInThePastOrSameDay;
use App\Repository\ReservationRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class ValidateReservationService implements ValidateReservationInterface
{
    public function __construct(private ReservationRepository $reservationRepository)
    {
    }


    /**
     * @throws ReservationConstraintsException
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function validate(Reservation $reservation): void
    {
        if($this->isInThePastOrToday($reservation)){
            throw new ReservationConstraintsException("Vous ne pouvez pas réserver une date dans le passé ou le jour-même");
        }

        if($this->hasAlreadyReservationThisWeek($reservation)){
            throw new ReservationConstraintsException("Vous avez déjà réservé cette semaine");
        }

        if($this->isComplete($reservation)){
            throw new ReservationConstraintsException("Il n'y a plus de réservation disponible pour ce jour");
        }
    }

    public function isInThePastOrToday(Reservation $reservation): bool
    {
        return $reservation->getBookedOn()->getTimestamp() <= (new \DateTime())->setTime(23,59,59)->getTimestamp();
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function hasAlreadyReservationThisWeek(Reservation $reservation): bool
    {
        return $this
            ->reservationRepository
            ->countByFoodTruckAndBetweenTwoDateTime(
                DateHelper::getFirstDayOfWeek($reservation->getBookedOn()),
                DateHelper::getLastDayOfWeek($reservation->getBookedOn()),
                $reservation
            ) > 0;
    }

    public function isFriday(Reservation $reservation): bool
    {
        return $reservation->getBookedOn()->format("N") == "5";
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function isComplete(Reservation $reservation): bool
    {
        if($this->isFriday($reservation)){
            return $this->reservationRepository->countByDate($reservation->getBookedOn()) >= 6;
        }

        return $this->reservationRepository->countByDate($reservation->getBookedOn()) >= 7;
    }
}
