<?php

namespace App\Service\Reservation;

use App\Entity\Reservation;

interface ValidateReservationInterface
{
    public function validate(Reservation $reservation): void;

    public function isInThePastOrToday(Reservation $reservation): bool;

    public function hasAlreadyReservationThisWeek(Reservation $reservation): bool;

    public function isFriday(Reservation $reservation): bool;

    public function isComplete(Reservation $reservation): bool;
}
