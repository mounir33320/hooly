<?php

namespace App\Service\Reservation;

use App\Entity\Reservation;
use App\Repository\ReservationRepository;

class ReservationService
{
    public function __construct(
        private ReservationRepository $reservationRepository,
        private ValidateReservationInterface $validateReservation
    ){}

    public function getAll(): array
    {
        return $this->reservationRepository->findAll();
    }

    public function save(Reservation $reservation): void
    {
        $this->validateReservation->validate($reservation);

        $this->reservationRepository->add($reservation);
    }

    public function delete(Reservation $reservation)
    {
        $this->reservationRepository->remove($reservation);
    }
}
