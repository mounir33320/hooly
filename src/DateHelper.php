<?php

namespace App;

class DateHelper
{
    public static function getFirstDayOfWeek(\DateTimeInterface $dateTime)
    {
        $numericDay = (int) $dateTime->format("N");
        return $dateTime->sub(new \DateInterval("P" .($numericDay - 1)."D"));
    }

    public static function getLastDayOfWeek(\DateTimeInterface $dateTime)
    {
        $numericDay = (int) $dateTime->format("N");
        return $dateTime->add(new \DateInterval("P" .(7 - $numericDay). "D"));
    }
}
