<?php

namespace App\Entity;

use App\Repository\ReservationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ReservationRepository::class)]
class Reservation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups([
        "read:collection:FoodTruck",
        "read:item:FoodTruck",
        "read:collection:Reservation",
        "read:item:Reservation"
    ])]
    private int $id;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups([
        "read:collection:FoodTruck",
        "read:item:FoodTruck",
        "read:collection:Reservation",
        "read:item:Reservation"
    ])]
    private \DateTimeImmutable $bookedOn;

    #[ORM\ManyToOne(targetEntity: FoodTruck::class, inversedBy: 'reservations')]
    #[Groups(["read:collection:Reservation", "read:item:Reservation"])]
    private $foodTruck;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBookedOn(): ?\DateTimeImmutable
    {
        return $this->bookedOn;
    }

    public function setBookedOn(\DateTimeImmutable $bookedOn): self
    {
        $this->bookedOn = $bookedOn;

        return $this;
    }

    public function getFoodTruck(): ?FoodTruck
    {
        return $this->foodTruck;
    }

    public function setFoodTruck(?FoodTruck $foodTruck): self
    {
        $this->foodTruck = $foodTruck;

        return $this;
    }
}
