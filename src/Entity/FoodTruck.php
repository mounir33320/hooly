<?php

namespace App\Entity;

use App\Repository\FoodTruckRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: FoodTruckRepository::class)]
class FoodTruck
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups([
        "read:collection:FoodTruck",
        "read:item:FoodTruck",
        "read:collection:Reservation",
        "read:item:Reservation"
    ])]
    private int $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups([
        "read:collection:FoodTruck",
        "read:item:FoodTruck",
        "read:collection:Reservation",
        "read:item:Reservation"
    ])]
    private string $name;

    #[ORM\OneToMany(mappedBy: 'foodTruck', targetEntity: Reservation::class, cascade: ["remove"])]
    #[Groups(["read:collection:FoodTruck", "read:item:FoodTruck"])]
    private $reservations;

    public function __construct()
    {
        $this->reservations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Reservation>
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations[] = $reservation;
            $reservation->setFoodTruck($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): self
    {
        if ($this->reservations->removeElement($reservation)) {
            // set the owning side to null (unless already changed)
            if ($reservation->getFoodTruck() === $this) {
                $reservation->setFoodTruck(null);
            }
        }

        return $this;
    }
}
