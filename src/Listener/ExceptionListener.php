<?php

namespace App\Listener;

use App\Exception\ReservationConstraintsException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ExceptionListener
{
    public function __construct(private string $appEnvironment)
    {
    }

    public function onKernelException(ExceptionEvent $event)
    {
        $throwable = $event->getThrowable();

        if (method_exists($throwable,"getStatusCode") && $throwable->getStatusCode() == 404){
            $jsonResponse = new JsonResponse(["success" => false, "message" => "Cette ressource n'existe pas"], Response::HTTP_NOT_FOUND);
            $event->setResponse($jsonResponse);
        }

        if ($throwable instanceof BadRequestHttpException){
            $jsonResponse = new JsonResponse(["success" => false, "message" => $throwable->getMessage()], Response::HTTP_BAD_REQUEST);
            $event->setResponse($jsonResponse);
        }

        if ($throwable instanceof ReservationConstraintsException){
            $jsonResponse = new JsonResponse(["success" => false, "message" => $throwable->getMessage()], Response::HTTP_BAD_REQUEST);
            $event->setResponse($jsonResponse);
        }

        if($this->appEnvironment != "dev"){
            $jsonResponse = new JsonResponse(["success" => false, "message" => "Server error occurred"], Response::HTTP_SERVICE_UNAVAILABLE);
            $event->setResponse($jsonResponse);
        }
    }
}
