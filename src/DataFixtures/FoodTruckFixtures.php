<?php

namespace App\DataFixtures;

use App\Entity\FoodTruck;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class FoodTruckFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        for($i = 1; $i <= 50; $i++){
            $foodTruck = (new FoodTruck())
                ->setName("Food Truck n°$i");
            $manager->persist($foodTruck);
        }

        $manager->flush();
    }
}
