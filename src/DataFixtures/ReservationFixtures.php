<?php

namespace App\DataFixtures;

use App\Entity\FoodTruck;
use App\Entity\Reservation;
use App\Repository\FoodTruckRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ReservationFixtures extends Fixture
{

    public function __construct(private FoodTruckRepository $foodTruckRepository)
    {
    }

    public function load(ObjectManager $manager)
    {
        $foodTruckList = $this->foodTruckRepository->findAll();

        for($i = 0; $i < 100; $i++){
            $reservation = (new Reservation())
                ->setBookedOn((new \DateTimeImmutable())->add(new \DateInterval("P" .$i. "D")))
                ->setFoodTruck($foodTruckList[rand(0,count($foodTruckList) - 1)]);
            $manager->persist($reservation);
        }

        $manager->flush();
    }
}
